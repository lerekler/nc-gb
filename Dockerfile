FROM centos:7

RUN yum install -y python3 python3-pip 

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

COPY python-api.py /python_api/python-api.py

CMD ["python3", "/python_api/python-api.py"]

